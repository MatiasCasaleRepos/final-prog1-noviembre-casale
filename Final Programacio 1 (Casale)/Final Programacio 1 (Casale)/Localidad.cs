﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class Localidad
    {
        public string nombreLocalidad { get; set; }
        public string provincia { get; set; }
        public int CP { get; set; }
    }
}
