﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public abstract class Pileta
    {
        public int codPileta { get; set; }
        public int cantLitrosPileta { get; set; }
        public double precioPileta { get; set; }
        public Materiales materialesPileta { get; set; }
        public Colores coloresPileta { get; set; }

        //Corrección: Seria correcto que sea un método virtual que siempre devuelva 0, de esa forma las piletas inflables por ejemplo no necesitan implementar el método.
        public abstract int CalcularPorcentajeDeDescuento();
    }

    public enum Materiales
    {
        Material, Lona, Inflables
    }

    public enum Colores
    {
        Azul, Celeste, Gris
    }
}
