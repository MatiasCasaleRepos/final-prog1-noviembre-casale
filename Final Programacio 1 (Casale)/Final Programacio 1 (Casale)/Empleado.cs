﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class Empleado : Usuario
    {
        public DateTime fechaNacimiento { get; set; }
        public Trunos turnoTrabajo { get; set; }
        public Areas areaTrabajo { get; set; }

        public override string DetallarDatosDeUsuario()
        {
            return base.DetallarDatosDeUsuario() + $"Turno {turnoTrabajo} - Área {areaTrabajo}";
        }

        public override string Mensajes()
        {
            //CORRECCIÓN: ESTO NO ES CORRECTO, LA FECHA DE NACIMIENTO NUNCA SERÁ LA MISMA QUE LA FECHA DE HOY.
            if (fechaNacimiento == DateTime.Today)
                return "¡Feliz cumpleaños compañero!";
            else
                return null;
        }
    }

    public enum Trunos
    {
        Mañana, Tarde
    }

    public enum Areas
    {
        Gerencia, Ventas, Instalación
    }
}
