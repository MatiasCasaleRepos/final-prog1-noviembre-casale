﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class CP_Empresa
    {
        List<Venta> ListaVentas = new List<Venta>();
        List<Cliente> ListaClientes = new List<Cliente>();
        List<Empleado> ListaEmpleados = new List<Empleado>();
        
        //CORRECCIÓN: NO SE APLICA POLIMORFISMO COMO LO VIMOS EN CLASE.
        List<PiletaInflable> ListaPiletasInflables = new List<PiletaInflable>();
        List<PiletaMaterial> ListaPiletasMaterial = new List<PiletaMaterial>();
        List<PiletaLona> ListaPiletasLona = new List<PiletaLona>();

        public List<Usuario> ObtenerListaUsuarios()
        {
            List<Usuario> usuarios = new List<Usuario>();

            foreach (Cliente cliente in ListaClientes)
                usuarios.Add(cliente);

            foreach (Empleado empleado in ListaEmpleados)
                usuarios.Add(empleado);

            return usuarios;
        }

        public List<Pileta> ObtenerListaPiletas()
        {
            List<Pileta> Piletas = new List<Pileta>();
        
            foreach (PiletaInflable piletaInflable in ListaPiletasInflables)
                Piletas.Add(piletaInflable);

            foreach (PiletaMaterial piletaMaterial in ListaPiletasMaterial)
                Piletas.Add(piletaMaterial);

            foreach (PiletaLona piletaLona in ListaPiletasLona)
                Piletas.Add(piletaLona);

            return Piletas;
        }

        public Resultado RegistrarVenta(int DNI, int codigoPileta)
        {
            int porcentajeDescuento = 0;
            double precioPileta = 0,
                   precioDescuento = 0;
            Resultado resultadoVenta = new Resultado();
            Venta ventaNueva = new Venta();
            List<Usuario> ListaUsuarios = ObtenerListaUsuarios();
            List<Pileta> ListaPiletas = ObtenerListaPiletas();

            //CORRECCIÓN: SI EL CÓDIGO DE PILETA NO EXISTE, EL MÉTODO First NOS DEVUELVE UN ERROR EN TIEMPO DE EJECUCIÓN ROMPIENDO EL PRIGRAMA.
            Pileta piletaEncontrada = ListaPiletas.First(x => x.codPileta == codigoPileta);
            //CORRECCÓN: LO MISMO QUE SE COMENTA POR EL CÓDIGO DE ARRIBA PASA CON EL DNI
            Usuario usuarioEncontrado = ListaUsuarios.First(x => x.DNI == DNI);

            bool tipoUsuario = usuarioEncontrado is Cliente;
            if (tipoUsuario == true)
            {
                //CORRECCIÓN: el índice de la lista no es el DNI, esto da muchos errores, por el tamaño del DNI podría dar siempre un error de "indice fuera del rango"
                //ES IMPORTANTE COMPRENDER ESTO, EL DNI NO ES EL INDICE, EN NINGUNA LISTA SUCEDE ESTO.
                ListaClientes[usuarioEncontrado.DNI].fechaInstalacion = DateTime.Today;
                ListaClientes[usuarioEncontrado.DNI].codPiletaInstalada = codigoPileta;
                ventaNueva.DNIComprador = DNI;
                ventaNueva.fechaVenta = DateTime.Today;
                ventaNueva.ImporteTotal = piletaEncontrada.precioPileta;
                //CORRECCIÓN: esto no está bien, el valor siempre será 1, LO VIMOS MUCHAS VECES EN CLASE
                ventaNueva.CodVenta++;
                ListaVentas.Add(ventaNueva);

                resultadoVenta.condicionResultado = true;
                resultadoVenta.Mensaje = "Se registró la nueva pileta del cliente";
                return resultadoVenta;
            }
            else
            {
                tipoUsuario = usuarioEncontrado is Empleado;
                if (tipoUsuario == true)
                {
                    porcentajeDescuento = piletaEncontrada.CalcularPorcentajeDeDescuento();
                    precioDescuento = piletaEncontrada.precioPileta*porcentajeDescuento/100;
                    precioPileta = piletaEncontrada.precioPileta - precioDescuento;
                    ventaNueva.DNIComprador = DNI;
                    ventaNueva.fechaVenta = DateTime.Today;
                    ventaNueva.ImporteTotal = precioPileta;
                    //Corrección: esto no está bien, el valor siempre será 1
                    ventaNueva.CodVenta++;
                    ListaVentas.Add(ventaNueva);

                    resultadoVenta.condicionResultado = true;
                    resultadoVenta.Mensaje = "El registro se realizó correctamente";
                    return resultadoVenta;
                }
                else
                {
                    Cliente nuevoCliente = new Cliente();
                    nuevoCliente.DNI = DNI;
                    nuevoCliente.codPiletaInstalada = codigoPileta;
                    //CORRECCIÓN: FALTA REGISTRAR LA FECHA DE INSTALACIÓN DE LA PILETA.
                    ListaClientes.Add(nuevoCliente);
                    resultadoVenta.condicionResultado = true;
                    resultadoVenta.Mensaje = "Se dio de alta la venta y el cliente, recuerde que debe completar sus datos";
                    return resultadoVenta;
                }
            }
        }
    }
}
