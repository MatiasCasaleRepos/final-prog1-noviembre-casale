﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class PiletaMaterial : Pileta
    {
        //CORRECCIÓN: LAS VARIABLES NO SE DEFINEN COMO SOLICITAMOS SIEMPRE, NO SE USA CAMEL CASE
        public int largo { get; set; }
        public int ancho { get; set; }
        public int Profundidad { get; set; }
        public bool filtroDisponible { get; set; }
        public bool trampolinDisponible { get; set; }
        public int cantEscaleras { get; set; }

        public override int CalcularPorcentajeDeDescuento()
        {
            if (Profundidad > 1.5 && trampolinDisponible == true)
                return 10;
            else
                return 5;
        }
    }
}
