﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class Venta
    {
        public int CodVenta { get; set; }
        public int DNIComprador { get; set; }
        public DateTime fechaVenta { get; set; }
        public double ImporteTotal { get; set; }
    }
}
