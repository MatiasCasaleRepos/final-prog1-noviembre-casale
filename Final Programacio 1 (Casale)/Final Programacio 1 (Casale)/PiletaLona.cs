﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class PiletaLona : Pileta
    {
        public int alto { get; set; }
        public int ancho { get; set; }
        public int Profundidad { get; set; }
        public bool filtroDisponible { get; set; }
        public bool cubrePiletasDisponible { get; set; }

        public override int CalcularPorcentajeDeDescuento()
        {
            return 8;
        }
    }
}
