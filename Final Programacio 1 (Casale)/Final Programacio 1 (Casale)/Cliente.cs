﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public class Cliente : Usuario
    {
        public int codPiletaInstalada { get; set; }
        public DateTime fechaInstalacion { get; set; }

        //CORRECCIÓN: NO SE DEBE SOBREESCRIBIR EL MÉTODO BASE SI NO SE CAMBIA EL COMPORTAMIENTO O SE LE AGREGA COMPORTAMIENTO AL MISMO
        public override string DetallarDatosDeUsuario()
        {
            return base.DetallarDatosDeUsuario();
        }

        public override string Mensajes()
        {
            //CORRECCIÓN: ESTA CONDIFICÓN NO FUNCIONA, SI LA FEHCA DE INSTALACIÓN SE REGISTRÓ A LAS 18:00, EL DIA QUE SE CUMPLA UN AÑO ESTE IF NO FUNCIONA, PORQUE EL TODAY DEVUELVE EL DÍA PERO CON 00HS
            if (fechaInstalacion.AddYears(1) == DateTime.Today)
                return "¡Felicitaciones por su primer aniversario de instalación!";
            else
                return null;
        }
    }
}
