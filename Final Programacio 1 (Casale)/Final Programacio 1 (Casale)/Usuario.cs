﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Programacio_1__Casale_
{
    public abstract class Usuario
    {
        //Corrección: Mantener una correlación en el nombramiento de las variables.
        public int DNI { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int telefono { get; set; }
        public string  direccion { get; set; }
        public Localidad localidad { get; set; }

        public virtual string DetallarDatosDeUsuario()
        {
            return $"{apellido}, {nombre} - Telefono: {telefono} - {localidad.provincia}, {localidad.nombreLocalidad}, {direccion}";
        }

        public abstract string Mensajes();
    }
}
